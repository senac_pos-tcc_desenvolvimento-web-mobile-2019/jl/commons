<p align="center">
    <img  alt="Growth" title="#Growth" src="assets/bannerCommons.svg" />
</p>

<h1 align="center"> 
	Growth: AgroTreinamentos online 🌾 
</h1>
<p align="center"> 🌿 Plataforma de treinamentos online focado no Agro.  🌱 </p>

<img src="https://img.shields.io/badge/web-react-blue">
<img src="https://img.shields.io/badge/mobile-react%20native-blueviolet">
<img src="https://img.shields.io/badge/node-12.17.0-green">
<img src="https://img.shields.io/badge/backend-firebase-orange">

<h3 align="center"> 
	🚧 Status do projeto🚧
</h3>

<p align="center">
  <img src="https://img.shields.io/badge/versão-1.0.0-yellow"> - under construction
</p>


Tabela de conteúdos
=================
<!--ts-->
   * [Sobre o projeto](#-sobre-o-projeto)
   * [Sobre o pacote commons do Growth](#-sobre-o-pacote-commons-do-growth)
   * [Como utilizar o pacote](#-como-utilizar-o-pacote)
   * [Funcionalidades](#%EF%B8%8F-funcionalidades)
   * [Autores](#-autores)
   * [Licença](#-licença)
<!--te-->

## 📋 Sobre o projeto
O Growth é uma plataforma de treinamentos online pensada para atender o Agro. Composta por aplicativo mobile e ambiente web, proporciona um espaço para centralização de tutoriais sobre as ferramentas e auxilia no processo de replicação de treinamentos entre os usuários.

O projeto é composto por 3 repositórios que se complementam, abrangendo assim ambiente web e mobile. 

## 📋 Sobre o pacote commons do Growth
Este projeto tem o intuito de servir como um pacote NPM para os projetos <a href="https://gitlab.com/senac_pos-tcc_desenvolvimento-web-mobile-2019/jl/web">web</a> e <a href="https://gitlab.com/senac_pos-tcc_desenvolvimento-web-mobile-2019/jl/app">mobile</a> do Growth, encapsulando vários recursos e funcionalidades em comum entre as duas versões.

O projeto oferece ferramentas para autenticação, gerenciamento dos dados de contexto da aplicação, camada de `models` (já plugados ao <a href="https://firebase.google.com/products/firestore">Cloud Firestore</a>), além de alguns outros utilitários, distribuídos em 4 módulos através do nome `@growth/commons`: `Auth`,`Context`, `Models` e `Utils`.

## 📦 Como utilizar o pacote

Não há um deploy do pacote final, portanto, é necessário adicionar a dependência utilizando o endereço do repositório `git`, ou o caminho local do projeto (após `clone` do repositódio):

```bash
# Adicionar através do endereço do repositório
$ yarn add git@gitlab.com:senac_pos-tcc_desenvolvimento-web-mobile-2019/jl/web.git

# Adicionar através do caminho local
$ yarn add ../caminho/local/onde/foi/clonado

```

Adicionada a dependência, basta importar o módulo e utilizar suas funções:

```javascript
import { Auth, Context, Models, Utils } from '@growth/commons';
```

## ⚙️ Funcionalidades

- **Módulo `Auth`:** ferramentas de autenticação e manipulação de usuários (<a href="https://gitlab.com/senac_pos-tcc_desenvolvimento-web-mobile-2019/jl/commons/-/tree/master/auth">ver documentação</a>);
- **Módulo `Context`:** ferramentas de gerenciamento de e acesso aos dados de contexto global da aplicação (<a href="https://gitlab.com/senac_pos-tcc_desenvolvimento-web-mobile-2019/jl/commons/-/tree/master/context">ver documentação</a>);
- **Módulo `Models`:** classes da camada de modelo da aplicação (<a href="https://gitlab.com/senac_pos-tcc_desenvolvimento-web-mobile-2019/jl/commons/-/tree/master/models">ver documentação</a>);
- **Módulo `Utils`:** métodos utilitários gerais (<a href="https://gitlab.com/senac_pos-tcc_desenvolvimento-web-mobile-2019/jl/commons/-/tree/master/utils">ver documentação</a>);


## 🦸 Autores

<table>
  <tr>
    <td align="center"><a href="https://gitlab.com/lekkinhah"><img style="border-radius: 50%;" src="https://pt.gravatar.com/userimage/186334662/ec308d4832e83fdc97fbb724d6f69a70.jpg" width="100px;" alt=""/><br /><sub><b>Letícia Vargas</b></sub></a><br /> </td>
    <td align="center"><a href="https://gitlab.com/jeferson.oliveira"><img style="border-radius: 50%;" src="https://pt.gravatar.com/userimage/16093557/f396bcb89e84b8e858ff92b5c5af91fd.jpeg" width="100px;" alt=""/><br /><sub><b>Jeferson Oliveira</b></sub></a><br /></td>
    
  </tr>

</table>


## 📝 Licença

Este projeto esta sobe a licença [MIT](./LICENSE).

Feito com ❤️ por Letícia Vargas e Jeferson Oliveira 👋🏽

---
