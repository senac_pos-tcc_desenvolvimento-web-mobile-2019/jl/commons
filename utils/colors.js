export default {
  OPACITY: {
    '100': 'FF',
    '95': 'F2',
    '90': 'E6',
    '85': 'D9',
    '80': 'CC',
    '75': 'BF',
    '70': 'B3',
    '65': 'A6',
    '60': '99',
    '55': '8C',
    '50': '80',
    '45': '73',
    '40': '66',
    '35': '59',
    '30': '4D',
    '25': '40',
    '20': '33',
    '15': '26',
    '10': '1A',
    '5': '0D',
    '1': '03',
    '0': '00'
  },

  opacity: (color, opacity) => {
    return color + Number(Math.round(255*opacity)).toString(16).toUpperCase();
  },

  WHITE: '#FFFFFF',

  BLACK: '#000000',
  BLACK_LIGHT: '#AEAEAE',

  ERROR: '#FF0000',
  ERROR_LIGHT: '#FFAEAE',

  GOLD: '#FBBB18',

  PRIMARY: '#009688',
  PRIMARY_DARK: '#005C53',

  BACKGROUND: '#F9F9F9',

  NONE: 'transparent',
};