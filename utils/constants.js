const ROLE = {
  CLIENT: 'client',
  EMPLOYEE: 'employee',
  ADMIN: 'admin'
};

const PROGRESS_STATUS = {
  PENDING: 'pending',
  DONE: 'done'
};

const PUBLISH_STATUS = {
  DRAFT: 'draft',
  READY: 'ready',
  PUBLISHED: 'published'
};

export { ROLE, PROGRESS_STATUS, PUBLISH_STATUS };
export default { ROLE, PROGRESS_STATUS, PUBLISH_STATUS };