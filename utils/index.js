import Constants from './constants';
import Colors from './colors';
import Strings from './strings';

export { Constants };
export { Colors };
export { Strings };

export default {
  Constants,
  Colors,
  Strings
};