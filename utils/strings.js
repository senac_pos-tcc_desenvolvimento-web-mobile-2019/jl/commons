export default {
  containsSearch: (targetString, searchString) => {
    const terms = searchString.trim().toLocaleLowerCase().split(' ').filter(term => term.trim().length);
    return terms.every(term => targetString.trim().toLocaleLowerCase().includes(term));
  },

  initials: (targetString) => {
    const parts = targetString.trim().toLocaleUpperCase().split(' ').filter(part => part.trim().length);
    return parts.map((part, index)=> (index == 0 || index+1 == parts.length) ? part[0] : null).join('');
  }
};