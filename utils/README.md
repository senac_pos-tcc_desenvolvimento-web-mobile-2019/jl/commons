# Módulo `Utils`

Contém definições de constantes e funções auxiliares de utilidade geral da aplicação.

```javascript
import { Utils } from '@growth/commons';
// ou
import Utils from '@growth/commons/utils';
```

## `Colors`

```jsx
import { Colors } from '@growth/commons/utils';

console.log('Error color is:', Colors.ERROR); // will output '#FF0000';
```

Conjunto de constantes com valores das cores da aplicação, no formato hexadecimal. São elas:

| Constante | Valor |
| --------- | ----- |
| WHITE | `#FFFFFF` |
| BLACK | `#000000` |
| BLACK_LIGHT | `#AEAEAE` |
| ERROR | `#FF0000` |
| ERROR_LIGHT | `#FFAEAE` |
| GOLD | `#FBBB18` |
| PRIMARY | `#009688` |
| PRIMARY_DARK | `#005C53` |
| BACKGROUND | `#F9F9F9` |

## `Constants`

```jsx
import { Constants } from '@growth/commons/utils';

console.log('A client role is:', ROLE.CLIENT); // will output 'client';
```

Conjunto de constantes com alguns valores chaves da aplicação. São elas:

| Grupo | Constantes |
| ----- | ---------- |
| ROLE | `CLIENT`, `EMPLOYEE`, `ADMIN` |
| PROGRESS_STATUS | `PENDING`, `DONE` |
| PUBLISH_STATUS | `DRAFT`, `READY`, `PUBLISHED` |


## `Strings`

Métodos auxiliares para manipulação de `strings`.

- ### `containsSearch (targetString, searchString)`
Verifica se um texto alvo `targetString` contém os termos de busca `searchString`, total ou parcialmente. O método verifica cada palavra em separado dos termos de busca (ignorando espaços e indiferente à maiúsculas/minúsculas), e retorna `true` no caso de todas as palavras da busca estarem contidas no texto alvo.

```jsx
import { containsSearch } from '@growth/commons/utils';

// Will return true:
containsSearch('THIS target string', 'th'); // partial match for "THIS"
containsSearch('THIS target string', 'this'); // total match for "THIS"
containsSearch('THIS target string', 'str'); // partial match for "string"
containsSearch('THIS target string', 'th sTR'); // partial match for "THIS" and for "string"
containsSearch('THIS target string', 'stR get'); // partial match for "string" and for "target"

// Will return false:
containsSearch('THIS target string', 'thiss'); // no match due to an aditional "s"
containsSearch('THIS target string', 'thargetstring'); //no match for those two words togheter
containsSearch('THIS target string', 'target search'); // total match for "target" but no match for "search"
```

---
- ### `initials (targetString)`
Retorna as iniciais  da primeira e última palavra do texto alvo `targetString`, em letras maiúsculas. Se houver somente uma palavra, só uma letra será retornada.

```jsx
import { initials } from '@growth/commons/utils';

containsSearch('THIS target string'); // will return "TS"
containsSearch('THIS target'); // will return "TT"
containsSearch('target'); // will return "T"
```