import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';

export { auth };
export { firestore };

export default {
  auth,
  firestore
};