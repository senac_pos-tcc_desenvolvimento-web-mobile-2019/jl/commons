import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

firebase.initializeApp({
  apiKey: "AIzaSyDxWtSHNCaxcF0701tzPTL4aRwfiYFANKk",
  authDomain: "growth-c1cc5.firebaseapp.com",
  databaseURL: "https://growth-c1cc5.firebaseio.com",
  projectId: "growth-c1cc5",
  storageBucket: "growth-c1cc5.appspot.com",
  messagingSenderId: "595988493859",
  appId: "1:595988493859:web:2443f398bae20986f56396"
});

const firebaseAuth = firebase.auth();
const firebaseFirestore = firebase.firestore();

export function auth() {
  return firebaseAuth;
}

export function firestore() {
  return firebaseFirestore;
}

export default {
  auth,
  firestore
};