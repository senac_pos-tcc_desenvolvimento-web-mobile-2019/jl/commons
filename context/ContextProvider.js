import React, { useReducer, useMemo, useCallback, useEffect } from 'react';

import Reducer, { getInitialState, ActionType } from './reducer';

import { auth } from '../firebase';
import { signOut } from '../auth';
import { User, Course, Track } from '../models';


export const Context = React.createContext();

export const ContextProvider = Context.Provider;

export const useContextValue = () => {
  const [state, dispatch] = useReducer(Reducer, getInitialState());

  const value = useMemo(() => ({ state, dispatch }), [state, dispatch]);

  const initializeUsers = useCallback(users => {
    console.log('[COMMONS/CONTEXT] initializing users with a list of ' + users.length);
    dispatch({ type: ActionType.INITIALIZE_USERS, payload: users })
  }, [dispatch]);

  const initializeCourses = useCallback(courses => {
    console.log('[COMMONS/CONTEXT] initializing courses with a list of ' + courses.length);
    dispatch({ type: ActionType.INITIALIZE_COURSES, payload: courses })
  }, [dispatch]);

  const initializeTracks = useCallback(tracks => {
    console.log('[COMMONS/CONTEXT] initializing tracks with a list of ' + tracks.length);
    dispatch({ type: ActionType.INITIALIZE_TRACKS, payload: tracks })
  }, [dispatch]);

  useEffect(() => {
    let userRole, userUnsubscriber;
    let modelUnsubscribers = [];

    const authUnsubscriber = auth().onAuthStateChanged((authUser) => {
      if (authUser) {
        console.log(`[COMMONS/CONTEXT] ${authUser.email} is authenticated at the moment`);
        userUnsubscriber = User.find(authUser.uid).onSnapshot({ includeMetadataChanges: true }, (doc) => {
          console.log(`[COMMONS/CONTEXT] user snapshot received (${doc.metadata.fromCache ? 'from cache' : 'up to date'}, ${doc.metadata.hasPendingWrites ? 'with pendings' : 'without pendings'})`);
          const user = User.fromDoc(doc);

          if (!user) {
            console.log(`[COMMONS/CONTEXT] user has no data, signing out...`);
            signOut();
          } else {
            const newMetadata = !user.__fromCache || user.__hasPendingWrites;
            if (!newMetadata) {
              console.log(`[COMMONS/CONTEXT] user metadata is old, ignoring it...`);
            } else {
              if (!user.active) {
                console.log(`[COMMONS/CONTEXT] user is inactive, signing out...`);
                signOut();
              } else {
                user.__authUser = authUser;
                dispatch({ type: ActionType.INITIALIZE_USER, payload: user });
                if (userRole != user.role) {
                  console.log(`[COMMONS/CONTEXT] user role changed from "${userRole}" to "${user.role}", re-subscribing...`);
                  userRole = user.role;
                  modelUnsubscribers.forEach(unsubscriber => unsubscriber());
                  if (user.role == 'admin') {
                    modelUnsubscribers.push(
                      User.onSnapshot(({ docs }) => initializeUsers(User.fromDocsList(docs)))
                    );
                    modelUnsubscribers.push(
                      Course.onSnapshot(({ docs }) => initializeCourses(Course.fromDocsList(docs)))
                    );
                    modelUnsubscribers.push(
                      Track.onSnapshot(({ docs }) => initializeTracks(Track.fromDocsList(docs)))
                    );
                  } else {
                    initializeUsers([]);
                    modelUnsubscribers.push(
                      Course.where('targets', 'array-contains', user.role).onSnapshot(({ docs }) => (
                        initializeCourses(Course.fromDocsList(docs))
                      ))
                    );
                    modelUnsubscribers.push(
                      Track.where('targets', 'array-contains', user.role).onSnapshot(({ docs }) => (
                        initializeTracks(Track.fromDocsList(docs))
                      ))
                    );
                  }
                }
              }
            }
          }
        });
      } else {
        console.log(`[COMMONS/CONTEXT] no one is authenticated at the moment`);
        modelUnsubscribers.forEach(unsubscriber => unsubscriber());
        userUnsubscriber && userUnsubscriber();
        setTimeout(() => {
          dispatch({ type: ActionType.RESET });
          userRole = undefined;
        }, 1000);
      }
    });

    return () => {
      modelUnsubscribers.forEach(unsubscriber => unsubscriber());
      userUnsubscriber && userUnsubscriber();
      authUnsubscriber && authUnsubscriber();
    };
  }, []);

  return value;
};