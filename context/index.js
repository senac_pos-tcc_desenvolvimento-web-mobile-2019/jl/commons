import { Context, ContextProvider, useContextValue } from './ContextProvider';
import { useStatus } from './hooks/status';
import { useCurrentUser, useUserCourseProgress, useUserTrackProgress } from './hooks/currentUser';
import { useUsers } from './hooks/users';
import { useCourses, usePublishedCourses, useTrackCourses, useUserCourses, useUserCoursesPending, useUserCoursesDone } from './hooks/courses';
import { useTracks, usePublishedTracks, useUserTracks, useUserTracksPending, useUserTracksDone, useTrackCoursesInfo } from './hooks/tracks';

export { Context, ContextProvider, useContextValue };
export { useStatus };
export { useCurrentUser, useUserCourseProgress, useUserTrackProgress };
export { useUsers };
export { useCourses, usePublishedCourses, useTrackCourses, useUserCourses, useUserCoursesPending, useUserCoursesDone };
export { useTracks, usePublishedTracks, useUserTracks, useUserTracksPending, useUserTracksDone, useTrackCoursesInfo };

export default {
  Context,
  ContextProvider,
  useContextValue,
  useStatus,
  useCurrentUser,
  useUserCourseProgress,
  useUserTrackProgress,
  useUsers,
  useCourses,
  usePublishedCourses,
  useTrackCourses,
  useUserCourses,
  useUserCoursesPending,
  useUserCoursesDone,
  useTracks,
  usePublishedTracks,
  useUserTracks,
  useUserTracksPending,
  useUserTracksDone,
  useTrackCoursesInfo
};