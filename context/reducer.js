export const getInitialState = (initialized = false) => ({
  user: { current: null, initialized },
  users: { current: [], initialized },
  courses: { current: [], initialized },
  tracks: { current: [], initialized }
});

export const ActionType = {
  INITIALIZE_USER: 'INITIALIZE_USER',
  INITIALIZE_USERS: 'INITIALIZE_USERS',
  INITIALIZE_COURSES: 'INITIALIZE_COURSES',
  INITIALIZE_TRACKS: 'INITIALIZE_TRACKS',
  RESET_USERS: 'RESET_USERS',
  RESET_COURSES: 'RESET_COURSES',
  RESET_TRACKS: 'RESET_TRACKS',
  RESET: 'RESET'
};

const Reducer = (state, action) => {
  const { type, payload } = action;
  switch (type) {
    case ActionType.INITIALIZE_USER:
      return {
        ...state,
        user: { current: payload, initialized: true }
      };
    case ActionType.INITIALIZE_USERS:
      return {
        ...state,
        users: { current: payload, initialized: true }
      };
    case ActionType.INITIALIZE_COURSES:
      return {
        ...state,
        courses: { current: payload, initialized: true }
      };
    case ActionType.INITIALIZE_TRACKS:
      return {
        ...state,
        tracks: { current: payload, initialized: true }
      };
    case ActionType.RESET_USERS:
      return {
        ...state,
        users: { current: [], initialized: false }
      };
    case ActionType.RESET_COURSES:
      return {
        ...state,
        courses: { current: [], initialized: false }
      };
    case ActionType.RESET_TRACKS:
      return {
        ...state,
        tracks: { current: [], initialized: false }
      };
    case ActionType.RESET:
      return getInitialState(true);
    default:
      throw new Error(`"${type}" is not a valid action`);
  }
};

export default Reducer;