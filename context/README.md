# Módulo `Context`

Contém ferramentas para criação e acesso ao contexto global da aplicação. 

```javascript
import { Context } from '@growth/commons';
// ou
import Context from '@growth/commons/context';
```

O <a href="https://pt-br.reactjs.org/docs/context.html">`context`</a> monta e disponibiliza informações sobre o status de inicialização da aplicação, sobre a sessão atual, e as instâncias dos `models` disponíveis para o usuário autenticado, além de vários <a href="https://pt-br.reactjs.org/docs/hooks-intro.html">`hooks`</a> de acesso fácil a todos esses dados.

O módulo exporta os seguintes facilitadores:

## Implementação do contexto: `ContextProvider` e o `hook` `useContextValue`
O módulo utiliza-se da <a href="https://pt-br.reactjs.org/docs/context.html">API Context do React</a> para definir um provedor global de contexto. Dessa forma, todos os componentes dentro do `provider` terão acesso ao mesmo contexto. O valor deste contexto, por sua vez, é montado pelo hook `useContextValue`, que inicializa e retorna todas as informações globais pertinentes ao resto da aplicação.

Vale ressaltar que `useContextValue` sempre leva em conta o perfil do usuário autenticado durante a definição dos dados que populam ou não o contexto. Portando, listas de treinamentos, trilhas e usuários são carregadas no valor do contexto dinamicamente, olhando somente para aqueles registros que o usuário autenticado tem permissão para ver - treinamentos exclusivos de funcionários (role `employee`) não são colocados no contexto do acesso de um usuário comum, por exemplo, enquanto que todos os treinamentos, sem distinção, são carregados sempre no contexto do acesso de um usuário administrador (role `admin`). Além disso, `useContextValue` está escutando a todo momento por mudanças nos dados de maneira transparente, e atualizando o contexto com o conjunto das informações mais recentes, sempre que necessário.

```jsx
import { ContextProvider, useContextValue } from '@growth/commons/context';

function MyApp() {
  const contextValue = useContextValue();

  return (
    <ContextProvider value={contextValue}>
        <MyComponent />
    </ContextProvider>
  );
};
```

Assim, `MyComponent` (e quaisquer outros componentes abaixo dele ou no mesmo nível dele) terão acesso à todos os outros `hooks` do módulo.

##  Status dos dados do contexto: `useStatus`
Informa o status geral da inicialização dos dados do contexto, podendo ser `initializing` ou `ready`.

```jsx
import { useStatus } from '@growth/commons/context';

function MyComponent(props) {
  const status = useStatus();
  
  if (status == 'initializing')
    return <MyLoaderComponent />;
  else
    return <MyAppContentComponent />;
};
```

## Usuário autenticado

- ### `useCurrentUser()`
Entrega a instância do `model` <a href="https://gitlab.com/senac_pos-tcc_desenvolvimento-web-mobile-2019/jl/commons/-/tree/master/models#user">`User`</a> que representa o usuário autenticado no momento. Caso ainda não haja um usuário autenticado, o resultado será `undefined`.

## Lista de usuários

Hooks que entregam uma lista de instâncias do `model` <a href="https://gitlab.com/senac_pos-tcc_desenvolvimento-web-mobile-2019/jl/commons/-/tree/master/models#user">model `User`</a>, respeitando a disponibilidade para o usuário autenticado no momento - somente usuários administradores (role `admin`) tem aceso a outros usuários da plataforma.

O argumento opcional `filter` é uma função de callback, que recebe o resultado original do `hook` como parâmetro. O resultado da execução desta função será o resultado final entregue pelo `hook`:

```jsx
import { useUsers } from '@growth/commons/context';

function MyComponent(props) {
  const adminUsers = useUsers(function(users) {
    return users.filter(user => user.role == 'admin');
  });

  console.log('Admin users are:', adminUsers);
  ...
};
```

- ### `useUsers(filter[opcional])`
Entrega todos os usuários registrados no sistema.

## Listas de treinamentos
Hooks que entregam uma lista de instâncias do `model` <a href="https://gitlab.com/senac_pos-tcc_desenvolvimento-web-mobile-2019/jl/commons/-/tree/master/models#course">`Course`</a>, respeitando a disponibilidade para o usuário autenticado no momento - usuários administradores (role `admin`) tem todos treinamentos à disposição.

O argumento opcional `filter` é uma função de callback, que recebe o resultado original do `hook` como parâmetro. O resultado da execução desta função será o resultado final entregue pelo `hook`:

```jsx
import { usePublishedCourses } from '@growth/commons/context';

function MyComponent(props) {
  const coursesWithVideo = usePublishedCourses(function(publishedCourses) {
    return publishedCourses.filter(course => course.mediaType == 'video' && course.videoUrl);
  });

  console.log('Courses with a video:', coursesWithVideo);
  ...
};
```

- ### `useCourses(filter[opcional])`
Entrega todos os treinamentos disponíveis, independente de status de publicação.

---
- ### `usePublishedCourses(filter[opcional])`
Entrega todos os treinamentos disponíveis que estejam publicados (status `published`).

---
- ### `useTrackCourses(track, filter[opcional])`
Entrega todos os treinamentos disponíveis que estejam publicados (status `published`) e que fazem parte da trilha informada. Espera-se que `track` seja uma instância do `model` <a href="https://gitlab.com/senac_pos-tcc_desenvolvimento-web-mobile-2019/jl/commons/-/tree/master/models#track">`Track`</a>. 

---
- ### `useUserCourses(filter[opcional])`
Entrega todos os treinamentos disponíveis que estejam publicados (status `published`) e que o usuário autenticado tenha algum progresso registrado (status `pending` ou `done`).

---
- ### `useUserCoursesPending(filter[opcional])`
Entrega todos os treinamentos disponíveis que estejam publicados (status `published`) e que o usuário autenticado tenha marcado como em andamento (status `pending`).

---
- ### `useUserCoursesDone(filter[opcional])`
Entrega todos os treinamentos disponíveis que estejam publicados (status `published`) e que o usuário autenticado já tenha concluído (status `done`).

## Lista de trilhas
Hooks que entregam uma lista de instâncias do `model` <a href="https://gitlab.com/senac_pos-tcc_desenvolvimento-web-mobile-2019/jl/commons/-/tree/master/models#track">`Track`</a>, respeitando a disponibilidade para o usuário autenticado no momento - usuários administradores (role `admin`) tem todas trilhas à disposição. Importante ressaltar que trilhas que não tenham ao menos um treinamento publicado também não são consideradas publicadas por estes `hooks`, mesmo que a trilha apresente o status correto.

O argumento opcional `filter` é uma função de callback, que recebe o resultado original do `hook` como parâmetro. O resultado da execução desta função será o resultado final entregue pelo `hook`:

```jsx
import { usePublishedTracks } from '@growth/commons/context';

function MyComponent(props) {
  const todaysTracks = usePublishedTracks(function(publishedTracks) {
    const today = new Date();
    today.setUTCHours(0,0,0,0);
    return publishedTracks.filter(track => track.createdAt >= today);
  });

  console.log('Tracks created today:', todaysTracks);
  ...
};
```

- ### `useTracks(filter[opcional])`
Entrega todas as trilhas disponíveis, independente de status.

---
- ### `usePublishedTracks(filter[opcional])`
Entrega todas as trilhas disponíveis que estejam publicadas (status `published`).

---
- ### `useUserTracks(filter[opcional])`
Entrega todas as trilhas disponíveis que estejam publicadas (status `published`) e que o usuário autenticado tenha algum progresso registrado em algum dos treinamentos que compõe a trilha (status `pending` ou `done`).

---
- ### `useUserTracksPending(filter[opcional])`
Entrega todas as trilhas disponíveis que estejam publicadas (status `published`) e que o usuário autenticado tenha marcado como algum progresso (status `pending` ou `done`) nos treinamentos que compõe a trilha, mas nem todos foram concluídos ainda (status `done`).

---
- ### `useUserTracksDone(filter[opcional])`
Entrega todas as trilhas disponíveis que estejam publicadas (status `published`) e que o usuário autenticado tenha concluído (status `done`) todos os treinamentos que compõe a trilha.

## Dados dos treinamentos de uma trilha

- ### `useTrackCoursesInfo(track)`
Entrega um objeto com informações detalhadas dos cursos que compõe a trilha informada. Espera-se que `track` seja uma instância do `model` <a href="https://gitlab.com/senac_pos-tcc_desenvolvimento-web-mobile-2019/jl/commons/-/tree/master/models#track">`Track`</a>. o objeto retornado contém as seguintes propriedades:
  - `courses`: um `array` que contém a lista de instâncias do `model` <a href="https://gitlab.com/senac_pos-tcc_desenvolvimento-web-mobile-2019/jl/commons/-/tree/master/models#course">`Course`</a> que representam os treinamentos que compõe a trilha - apenas treinamentos publicados são considerados (status `published`);
  - `estimatedTime`: o tempo estimado todal da trilha, calculado a partir do somatório do tempo estimado (`estimatedTime`) definido nos treinamentos que compõe a trilha;

## Dados de progresso
Fornece detalhes do progresso do usuário autenticado em um treinamento específico ou uma trilha específica. Ambos os hooks contabilizam apenas os treinamentos e trilhas publicadas (status `published`), e considera como não publicadas as trilhas que não tenham ao menos um treinamento publicado.

- ### `useUserCourseProgress(course)`
Entrega um objeto contendo os dados de progresso do usuário autenticado no treinamento informado. Espera-se que `course` seja uma instância do `model` <a href="https://gitlab.com/senac_pos-tcc_desenvolvimento-web-mobile-2019/jl/commons/-/tree/master/models#course">`Course`</a>. O objeto retornado apresenta as seguintes propriedades:
  - `accessedAt`: a data do último acesso à página do treinamento;
  - `startedAt`: a data em que o treinamento foi dado como iniciado (status `pending`);
  - `doneAt`: a data em que o treinamento foi marcado como concluído (status `done`);
  - `status`: o status atual em relação ao treinamento (`pending` ou `done`);
  - `ratedAs`: o valor com o qual o treinamento foi avaliado pelo usuário;
---
- ### `useUserTrackProgress(track)`
Entrega um objeto contendo os dados de progresso do usuário autenticado na trilha informada. Espera-se que `track` seja uma instância do `model` <a href="https://gitlab.com/senac_pos-tcc_desenvolvimento-web-mobile-2019/jl/commons/-/tree/master/models#track">`Track`</a>. O objeto retornado apresenta as seguintes propriedades:
  - `done`: `array` contendo uma lista com dados de progresso dos treinamentos finalizados (status `done`), no mesmo formato do retorno de `useUserCourseProgress`;
  - `pending`: `array` contendo uma lista com dados de progresso dos treinamentos em andamento (status `pending`), no mesmo formato do retorno de `useUserCourseProgress`;
  - `startedAt`: a data em que a trilha foi iniciada (a menor data em que um treinamento da trilha foi dado como iniciado - status `pending`);
  - `doneAt`: a data em que a trilha foi finalizada (a maior data em que um treinamento da trilha foi marcado como concluído - status `done`);
  - `status`: o status do progresso da trilha (`done`, caso todos os treinamentos já tenham sido concluídos, ou `pending`, caso haja treinamentos com algum progresso, mas nem todos foram concluídos ainda);
  - `percentage`: o valor percentual do progresso, calculado a partir do total de treinamentos concluídos em relação ao total de treinamentos disponíveis na trilha;

