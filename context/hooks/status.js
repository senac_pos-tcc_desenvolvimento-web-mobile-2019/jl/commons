import { useContext } from 'react';

import { Context } from '../ContextProvider';


export const useStatus = () => {
  const { state: { user, users, courses, tracks } } = useContext(Context);

  if (user.initialized && users.initialized && courses.initialized && tracks.initialized)
    return 'ready';
  else
    return 'initializing';
};