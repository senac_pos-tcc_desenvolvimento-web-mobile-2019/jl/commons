import { useContext } from 'react';

import { Context } from '../ContextProvider';


export const useUsers = (filter) => {
  const { state: { users } } = useContext(Context);

  if (users.initialized)
    return filter ? filter(users.current) : users.current;

  return [];
};