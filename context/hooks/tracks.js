import { useContext } from 'react';

import { PUBLISH_STATUS, PROGRESS_STATUS } from '../../utils/constants';
import { Context } from '../ContextProvider';
import { usePublishedCourses, useUserCourses, useUserCoursesPending, useUserCoursesDone, useTrackCourses } from './courses';


export const useTracks = (filter) => {
  const { state: { tracks } } = useContext(Context);

  if (tracks.initialized)
    return filter ? filter(tracks.current) : tracks.current;

  return [];
};

export const usePublishedTracks = (filter) => {
  const { state: { tracks } } = useContext(Context);
  const publishedCoursesIds = usePublishedCourses(courses => courses.map(({ id }) => id));

  if (tracks.initialized && publishedCoursesIds.length) {
    const publishedTracks = tracks.current.filter(track => (
      track.status == PUBLISH_STATUS.PUBLISHED && !!track.courses && track.courses.some(({ id }) => publishedCoursesIds.includes(id))
    ));
    return filter ? filter(publishedTracks) : publishedTracks;
  }

  return [];
};

export const useUserTracks = (filter) => {
  const { state: { tracks } } = useContext(Context);
  const userCoursesIds = useUserCourses(courses => courses.map(({ id }) => id));

  if (tracks.initialized && userCoursesIds.length) {
    const userTracks = tracks.current.filter(track => (
      track.status == PUBLISH_STATUS.PUBLISHED && !!track.courses && track.courses.some(({ id }) => userCoursesIds.includes(id))
    ));
    return filter ? filter(userTracks) : userTracks;
  }

  return [];
};

export const useUserTracksPending = (filter) => {
  const { state: { tracks, user } } = useContext(Context);
  const publishedCoursesIds = usePublishedCourses(courses => courses.map(({ id }) => id));

  if (tracks.initialized && user.current && publishedCoursesIds.length) {
    const userTracks = tracks.current.filter(track => {
      if (track.courses && track.courses.length) {
        const trackCoursesIds = track.courses.reduce((ids, { id }) => publishedCoursesIds.includes(id) ? [...ids, id] : ids, []);
        if (trackCoursesIds && trackCoursesIds.length) {
          const progress = user.current.getCoursesGroupProgress(trackCoursesIds);
          return !!progress && progress.status == PROGRESS_STATUS.PENDING;
        }
      }
      return false;
    });
    return filter ? filter(userTracks) : userTracks;
  }

  return [];
};

export const useUserTracksDone = (filter) => {
  const { state: { tracks, user } } = useContext(Context);
  const publishedCoursesIds = usePublishedCourses(courses => courses.map(({ id }) => id));

  if (tracks.initialized && user.current && publishedCoursesIds.length) {
    const userTracks = tracks.current.filter(track => {
      if (track.courses && track.courses.length) {
        const trackCoursesIds = track.courses.reduce((ids, { id }) => publishedCoursesIds.includes(id) ? [...ids, id] : ids, []);
        if (trackCoursesIds && trackCoursesIds.length) {
          const progress = user.current.getCoursesGroupProgress(trackCoursesIds);
          return !!progress && progress.status == PROGRESS_STATUS.DONE;
        }
      }
      return false;
    });
    return filter ? filter(userTracks) : userTracks;
  }

  return [];
};;

export const useTrackCoursesInfo = (track) => {
  const courses = useTrackCourses(track);
  const estimatedTime = courses.reduce((total, course) => total + (course.estimatedTime || 0), 0);

  return { courses, estimatedTime };
};