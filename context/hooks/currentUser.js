import { useContext } from 'react';

import { ROLE, PUBLISH_STATUS, PROGRESS_STATUS } from '../../utils/constants';
import { Context } from '../ContextProvider';
import { useTrackCourses } from './courses';


export const useCurrentUser = () => {
  const { state: { user } } = useContext(Context);

  if (user.initialized)
    return user.current;

  return;
};

export const useUserCourseProgress = (course) => {
  const { state: { user } } = useContext(Context);

  if (user.initialized && user.current && !!course) {
    const courseProgress = user.current.getCourseProgress(course.id);
    if (courseProgress) {
      const { status, accessedAt, startedAt, doneAt, ratedAs } = courseProgress;
      return { status, accessedAt, startedAt, doneAt, ratedAs };
    }
  }

  return { };
};

export const useUserTrackProgress = (track) => {
  const { state: { user } } = useContext(Context);
  const trackCourses = useTrackCourses(track);

  if (user.initialized && user.current && trackCourses.length && !!track) {
    const trackCoursesIds = trackCourses.map(({ id }) => id);
    const progress = user.current.getCoursesGroupProgress(trackCoursesIds);
    if (progress) {
      progress.percentage = progress.done.length == trackCourses.length ? 100 : Math.round((progress.done.length * 100) / trackCourses.length);
      return progress;
    }
  }

  return { done: [], pending: [], percentage: 0 };
};