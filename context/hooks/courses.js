import { useContext } from 'react';

import { PUBLISH_STATUS } from '../../utils/constants';
import { Context } from '../ContextProvider';


export const useCourses = (filter) => {
  const { state: { courses } } = useContext(Context);

  if (courses.initialized)
    return filter ? filter(courses.current) : courses.current;

  return [];
};

export const usePublishedCourses = (filter) => {
  const { state: { courses } } = useContext(Context);

  if (courses.initialized) {
    const publishedCourses = courses.current.filter(({ status }) => (
      status == PUBLISH_STATUS.PUBLISHED
    ));
    return filter ? filter(publishedCourses) : publishedCourses;
  }

  return [];
};

export const useTrackCourses = (track, filter) => {
  const { state: { courses } } = useContext(Context);

  if (courses.initialized && !!(track && track.courses)) {
    const trackCoursesIds = track.coursesIds();
    const trackCourses = courses.current.filter(({ id, status }) => (
      status == PUBLISH_STATUS.PUBLISHED && trackCoursesIds.includes(id)
    ));
    return filter ? filter(trackCourses) : trackCourses;
  }

  return [];
};

export const useUserCourses = (filter) => {
  const { state: { user, courses } } = useContext(Context);

  if (courses.initialized && user.current) {
    const userCoursesIds = user.current.progressedCoursesIds();
    const userCourses = courses.current.filter(({ id, status }) => (
      status == PUBLISH_STATUS.PUBLISHED && userCoursesIds.includes(id)
    ));
    return filter ? filter(userCourses) : userCourses;
  }

  return [];
};

export const useUserCoursesPending = (filter) => {
  const { state: { user, courses } } = useContext(Context);

  if (courses.initialized && user.current) {
    const userCoursesIds = user.current.pendingCoursesIds();
    const userCourses = courses.current.filter(({ id, status }) => (
      status == PUBLISH_STATUS.PUBLISHED && userCoursesIds.includes(id)
    ));
    return filter ? filter(userCourses) : userCourses;
  }

  return [];
};

export const useUserCoursesDone = (filter) => {
  const { state: { user, courses } } = useContext(Context);

  if (courses.initialized && user.current) {
    const userCoursesIds = user.current.doneCoursesIds();
    const userCourses = courses.current.filter(({ id, status }) => (
      status == PUBLISH_STATUS.PUBLISHED && userCoursesIds.includes(id)
    ));
    return filter ? filter(userCourses) : userCourses;
  }

  return [];
};