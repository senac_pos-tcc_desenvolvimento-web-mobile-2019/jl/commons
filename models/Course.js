import FirestoreModel from './FirestoreModel';


export default class Course extends FirestoreModel {
  static collectionPath() { return 'courses'; }

  constructor(doc) {
    super(doc);
  }

  get date() {
    return this.updatedAt || this.createdAt;
  }

  static async computeNewRating(courseId, rating) {
    const doc = await this.find(courseId).get();
    const course = this.fromDoc(doc);
    if (course) {
      if (course.rating) {
        course.rating.count ++;
        course.rating.sum += rating;
      } else {
        course.rating = { count: 1, sum: rating };
      }
      course.update({ rating: course.rating }, true);
    }
  }
};