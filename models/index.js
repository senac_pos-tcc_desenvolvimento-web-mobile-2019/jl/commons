import User from './User';
import Course from './Course';
import Track from './Track';

export { User };
export { Course };
export { Track };

export default {
  User,
  Course,
  Track
};