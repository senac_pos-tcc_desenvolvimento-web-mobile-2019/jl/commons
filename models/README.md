# Módulo `Models`

Contém as classes que formam a camada de `model` da aplicação.

```javascript
import { Models } from '@growth/commons';
// ou
import Models from '@growth/commons/models';
```

Toda classe `model` extende da classe base `FirebaseModel`, que contém as principais funções que fazem com que um model represente diretamente um <a href="https://firebase.google.com/docs/firestore/data-model?hl=pt-br#documents">documento</a> em uma <a href="https://firebase.google.com/docs/firestore/data-model?hl=pt-br#collections">coleção</a> do <a href="https://firebase.google.com/products/firestore">Cloud Firestore</a>. Assim, além dos métodos próprios de cada classe, todo `model` também possui os mesmos métodos básicos para criação, busca, edição e exclusão de registros.

O módulo exporta as seguintes classes:

## `FirebaseModel`
Classe básica que representa um <a href="https://firebase.google.com/docs/firestore/data-model?hl=pt-br#documents">document</a> em uma <a href="https://firebase.google.com/docs/firestore/data-model?hl=pt-br#collections">collection</a>.

- ### `static collection()`
Retorna o nome da coleção que a classe representa. Espera-se que a classe concreta que extende `FirebaseModel` informe este nome através do método estático `collectionPath`.

- ### `static fromDoc(doc)`
Inicializa e retorna uma nova instância da classe do `model` que extende de `FirebaseModel`, a partir de um <a href="https://firebase.google.com/docs/reference/js/firebase.firestore.DocumentSnapshot">`DocumentSnapshot` da API do Firestore</a>.

- ### `static fromDocsList(docs)`
Inicializa e retorna uma lista de novas instâncias da classe do `model` que extende de `FirebaseModel`, a partir de uma lista de <a href="https://firebase.google.com/docs/reference/js/firebase.firestore.DocumentSnapshot">`DocumentSnapshot` da API do Firestore</a>.

- ### `static find(id)`
Retorna uma `promise` que devolve o <a href="https://firebase.google.com/docs/reference/js/firebase.firestore.DocumentSnapshot">`DocumentSnapshot`</a> encontrado para um ID de documento.

- ### `static async create(props)`
Método assíncrono que cria um novo documento na collection definida pela classe que extende de `FirebaseModel`, com as propriedades `props` informadas, e retorna o <a href="https://firebase.google.com/docs/reference/js/firebase.firestore.DocumentSnapshot">`DocumentSnapshot`</a> do documento criado.

Independentemente das propriedades informadas, todo documento criado por este método sempre ganha as propriedades `createdAt` e `createdBy`, contendo a data e o ID do `User` autenticado no momento da criação.

- ### `async update(props[opcional], anonymous[opcional])`
Método assíncrono que retorna uma `promise` que atualiza os dados do documento ao qual a instância atual representa. Caso o objeto de propriedades `props` seja informado, apenas este conteúdo será escrito no documento, do contrário, o conteúdo que será escrito é o valor atual de todas as propriedades da instância no momento do update.

Independentemente das propriedades informadas, todo documento atualizado por este método sempre ganha as propriedades `updatedAt` e `updatedBy`, contendo a data e o ID do `User` autenticado no momento da atualização, exceto se `anonymous` for sido definido como `true` - dessa forma, nenhuma das duas propriedades é criada ou alterada.

- ### `async delete()`
Método assíncrono que retorna uma `promise` que exclui o documento ao qual a instância atual representa.

Todo documento excluído por este método sempre ganha as propriedades `deletedAt` e `deletedBy`, contendo a data e o ID do `User` autenticado no momento da exclusão. Além disso, logo antes de ser excluído, uma cópia do documento é guardada em uma `collection` de backup, contendo o mesmo nome da `collection` do `model`, sucescedido de `_deleted` (ex.: `users_deleted` para `User`).

## `User`
Representa um <a href="https://firebase.google.com/docs/firestore/data-model?hl=pt-br#documents">document</a> na <a href="https://firebase.google.com/docs/firestore/data-model?hl=pt-br#collections">collection</a> de `users`. Uma instância de `User` contém os dados do perfil de um usuário na aplicação, e está atrelado a uma credencial do módulo de autenticação do Firebase.

- ### `coursesIds()`
Retorna um `array` com os IDs de todos os treinamentos (`Course`) que o usuário interagiu (se apenas visitou a página, se já marcou como iniciado, ou se já marcou como concluído).

- ### `pendingCoursesIds()`
Retorna um `array` contendo apenas os IDs dos treinamentos (`Course`) que o usuário marcou como iniciado (status `pending`) mas ainda não concluiu.

- ### `doneCoursesIds()`
Retorna um `array` contendo apenas os IDs dos treinamentos (`Course`) que o usuário já marcou como concluído (status `done`).

- ### `progressedCoursesIds()`
Retorna um `array` com os IDs dos treinamentos (`Course`) que o usuário já marcou algum progresso (concluído ou iniciado).

- ### `getCourseProgress(courseId)`
Retorna um objeto que representa o progresso do usuário no treinamento (`Course`) de ID informado. O objeto retornado contém as propriedades:
- `id`: o ID do treinamento;
- `accessedAt`: a data do último acesso à página do treinamento;
- `startedAt`: a data em que o treinamento dado como iniciado (status `pending`);
- `doneAt`: a data em que o treinamento foi marcado como concluído (status `done`);
- `status`: o status atual em relação ao treinamento (`pending` ou `done`);
- `ratedAs`: o valor com o qual o treinamento foi avaliado pelo usuário;

Caso o usuário não tenha interagido com o treinamento de ID informado, o método retornará `undefined`.

- ### `getCoursesGroupProgress(coursesIds)`
Retorna um objeto representando o progresso do usuário em um grupo de  IDs de treinamento (`Course`) informado. O objeto retornado contém as propriedades:
- `done`: array contendo uma lista com dados de progresso dos treinamentos finalizados (status `done`);
- `pending`: array contendo uma lista com dados de progresso dos treinamentos em andamento (status `pending`);
- `startedAt`: a menor data em que um treinamento do grupo foi dado como iniciado (status `pending`);
- `doneAt`: a maior data em que um treinamento do grupo foi marcado como concluído (status `done`);
- `status`: o status geral do progresso do grupo (`done`, caso todos os treinamentos já tenham sido concluídos, ou `pending`, caso haja treinamentos com algum progresso, mas nem todos foram concluídos ainda);

O objeto com os dados de progresso de cada treinamento nos arrays `done` e `pending` é o mesmo objeto retornado em pelo método `getCourseProgress`.

- ### `async setCourseAccess(courseId)`
Método assíncrono que retorna uma `promise` que marca um registro de acesso pelo usuário ao treinamento de ID informado. A data do momento de execução é definida como a data do acesso (`accessedAt`).

- ### `async setCourseProgress(courseId, status)`
Método assíncrono que retorna uma `promise` que marca um status de progresso do usuário para o treinamento de ID informado. A data do momento de execução é registrada como a data do progresso (`startedAt`, para status `pending`, `doneAt` para status `done`).

- ### `async setCourseRating(courseId, rating)`
Método assíncrono que retorna uma `promise` que define uma nota avaliativa que o usuário deu para o treinamento de ID informado. Ao definid uma nota por este método, a média do treinamento em questão e atualizada de acordo.

## `Course`
Representa um <a href="https://firebase.google.com/docs/firestore/data-model?hl=pt-br#documents">document</a> na <a href="https://firebase.google.com/docs/firestore/data-model?hl=pt-br#collections">collection</a> de `courses`. Uma instância de `Course` contém os dados de um treinamento na aplicação.

## `Track`
Representa um <a href="https://firebase.google.com/docs/firestore/data-model?hl=pt-br#documents">document</a> na <a href="https://firebase.google.com/docs/firestore/data-model?hl=pt-br#collections">collection</a> de `tracks`. Uma instância de `Track` contém os dados de uma trilha na aplicação, que agrupa um conjunto de treinamentos (`Course`).

- ### `coursesIds()`
Retorna um `array` com os IDs de todos os treinamentos (`Course`) que compõe a trilha.