import FirestoreModel from './FirestoreModel';


export default class Track extends FirestoreModel {
  static collectionPath() { return 'tracks'; }

  constructor(doc) {
    super(doc);
  }

  coursesIds() {
    return this.courses ? this.courses.map(course => course.id) : [];
  }
};