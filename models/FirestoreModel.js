import Firebase from '../firebase';


export default class FirestoreModel {
  constructor(doc) {
    this.__doc = doc;
    this.__fromCache = doc && doc.metadata && doc.metadata.fromCache;
    this.__hasPendingWrites = doc && doc.metadata && doc.metadata.hasPendingWrites;
    this.id = doc && doc.id;

    const data = doc && doc.data && doc.data();
    this.__toProps(data, this);
  }

  static fromDoc(doc) {
    if (!!doc && doc.exists)
      return new this(doc);
  }

  static fromDocsList(docs = []) {
    return docs.map(doc => this.fromDoc(doc));
  }

  static collection() {
    return Firebase.firestore().collection(this.collectionPath());
  }

  static find(id) {
    return this.collection().doc(id);
  }

  static where(...args) {
    return this.collection().where(...args);
  }

  static onSnapshot(...args) {
    return this.collection().onSnapshot(...args);
  }

  static async create(props = {}) {
    const { id, ...data } = props;
    data.createdAt = new Date();
    data.createdBy = Firebase.auth().currentUser && Firebase.auth().currentUser.uid;

    if (id) {
      await this.collection().doc(id).set(data);
      return this.collection().doc(id).get();
    } else {
      const recordRef = await this.collection().add(data);
      return recordRef.get();
    }
  }

  __toProps(data, target) {
    for(const [propKey, propValue] of Object.entries(data)) {
      target[propKey] = (!!propValue && propValue.toDate) ? propValue.toDate() : propValue;
    }
  }

  __toData() {
    const data = {};
    for(const [propKey, propValue] of Object.entries(this)) {
      if (typeof propValue != 'function' && !propKey.startsWith('__') && propKey != 'id')
        data[propKey] = propValue;
    }
    return data;
  }

  update(props, anonymus) {
    const data = props || this.__toData();
    if (!anonymus) {
      data.updatedAt = new Date();
      data.updatedBy = Firebase.auth().currentUser && Firebase.auth().currentUser.uid;
    }
    this.__toProps(data, this);
    return this.__doc.ref.update(data);
  }

  delete() {
    const data = this.__toData();
    data.deletedAt = new Date();
    data.deletedBy = Firebase.auth().currentUser && Firebase.auth().currentUser.uid;
    this.__createBackup('deleted', data, this.id);
    return this.__doc.ref.delete();
  }

  async __createBackup(backupName, data, id) {
    const backupCollection = Firebase.firestore().collection(this.__doc.ref.parent.id + '_' + backupName);
    if (id) {
      await backupCollection.doc(id).set(data);
      return backupCollection.doc(id).get();
    } else {
      const recordRef = await backupCollection.add(data);
      return recordRef.get();
    }
  }

};