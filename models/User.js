import FirestoreModel from './FirestoreModel';
import Course from './Course';
import { ROLE, PROGRESS_STATUS } from '../utils/constants';


export default class User extends FirestoreModel {
  static collectionPath() { return 'users'; }

  constructor(doc) {
    super(doc);

    if (this.courses)
      this.courses.forEach(course => this.__toProps(course, course));
  }

  static initialRole(email) {
    return email.includes('checkplant.com.br') ? ROLE.EMPLOYEE : ROLE.CLIENT;
  }

  get accessedAt() {
    if (this.__authUser)
     return new Date(this.__authUser.metadata.lastSignInTime);
  }

  coursesIds() {
    return this.courses ? this.courses.map(course => course.id) : [];
  }

  pendingCoursesIds() {
    return !this.courses ? [] : this.courses.reduce((ids, course) => {
      if (course.status == PROGRESS_STATUS.PENDING) ids.push(course.id);
      return ids;
    }, []);
  }

  doneCoursesIds() {
    return !this.courses ? [] : this.courses.reduce((ids, course) => {
      if (course.status == PROGRESS_STATUS.DONE) ids.push(course.id);
      return ids;
    }, []);
  }

  progressedCoursesIds() {
    return !this.courses ? [] : this.courses.reduce((ids, course) => {
      if (!!course.status) ids.push(course.id);
      return ids;
    }, []);
  }

  getCoursesGroupProgress(coursesIds = []) {
    if (this.courses && this.courses.length) {
      const progress = this.courses.reduce((progress, { id, status, startedAt, doneAt }) => {
        if (coursesIds.includes(id)) {
          if (status == PROGRESS_STATUS.DONE)
            progress.done.push(id);
          if (status == PROGRESS_STATUS.PENDING)
            progress.pending.push(id);

          if (!progress.startedAt || (!!startedAt && startedAt < progress.startedAt))
            progress.startedAt = startedAt;
          if (!progress.doneAt || (!!doneAt && doneAt > progress.doneAt))
            progress.doneAt = doneAt;
        }
        return progress;
      }, { done: [], pending: [] });

      if (progress.done.length == coursesIds.length)
        progress.status = PROGRESS_STATUS.DONE;
      else if (progress.done.length || progress.pending.length)
        progress.status = PROGRESS_STATUS.PENDING;

      return progress;
    }
  }

  getCourseProgress(courseId) {
    return this.courses && this.courses.find(({ id }) => id == courseId);
  }

  async setCourseProgress(courseId, status) {
    if (!this.courses)
      this.courses = [];

    let progress = this.getCourseProgress(courseId);
    if (!progress) {
      progress = { id: courseId };
      this.courses.push(progress);
    }

    progress.status = status;
    if (status == PROGRESS_STATUS.PENDING)
      progress.startedAt = new Date();
    if (status == PROGRESS_STATUS.DONE)
      progress.doneAt = new Date();

    return this.update({ courses: this.courses });
  }

  async deleteCourseProgress(courseId) {
    if (this.courses) {
      this.courses = this.courses.filter(({ id }) => id != courseId);
    }

    return this.update({ courses: this.courses });
  }

  async setCourseRating(courseId, rating) {
    if (!this.courses)
      return;

    const progress = this.getCourseProgress(courseId);
    if (!progress)
      return;

    Course.computeNewRating(courseId, rating);
    progress.ratedAs = rating;
    return this.update({ courses: this.courses });
  }

  async setCourseAccess(courseId) {
    if (!this.courses)
      this.courses = [];

    let progress = this.getCourseProgress(courseId);
    if (!progress) {
      progress = { id: courseId };
      this.courses.push(progress);
    }

    progress.accessedAt = new Date();
    return this.update({ courses: this.courses });
  }
};