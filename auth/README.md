# Módulo `Auth`

Contém ferramentas necessárias para autenticação e manipulação de usuários.

```javascript
import { Auth } from '@growth/commons';
// ou
import Auth from '@growth/commons/auth';
```

O módulo exporta as seguintes funções:

## `signUp(email, password, name)`
Método assíncrono que inscreve um novo usuário no sistema com o e-mail/senha e nome informados, inicializando a sessão. O método cria as credenciais de acesso no módulo de autenticação do Firebase, e retorna uma instância do `model` <a href="https://gitlab.com/senac_pos-tcc_desenvolvimento-web-mobile-2019/jl/commons/-/blob/master/models/README.md#user">`User`</a>, representando o usuário criado. Os erros do processo são mantidos conforme a documentação da <a href="https://firebase.google.com/docs/reference/js/firebase.auth.Error">API do Firebase</a>.

```javascript
import { Auth } from '@growth/commons';

Auth.signUp('new@user.com', 'new-user-password', 'A New User')
    .then(user => console.log('User created:', user))
    .catch(error => console.log('Sign up error:', error));
```

## `signIn(email, password)`
Método assíncrono que inicializa a sessão de um usuário já existente no sistema, através do e-mail/senha informados. O método retorna uma instância do `model` <a href="https://gitlab.com/senac_pos-tcc_desenvolvimento-web-mobile-2019/jl/commons/-/blob/master/models/README.md#user">`User`</a>, representando o usuário autenticado. Os erros do processo são mantidos conforme a documentação da <a href="https://firebase.google.com/docs/reference/js/firebase.auth.Error">API do Firebase</a>.

```javascript
import { Auth } from '@growth/commons';

Auth.signIn('existent@user.com', 'user-password')
    .then(user => console.log('User authenticated:', user))
    .catch(error => console.log('Sign in error:', error));
```

## `resetPassword(email)`
Método assíncrono que envia um e-mail de recuperação de senha do usuário com o endereço informado. Os erros do processo são mantidos conforme a documentação da <a href="https://firebase.google.com/docs/reference/js/firebase.auth.Error">API do Firebase</a>.

```javascript
import { Auth } from '@growth/commons';

Auth.resetPassword('recover@user.com')
    .then(() => console.log('E-mail sent'))
    .catch(error => console.log('Reset password error:', error));
```

## `signOut()`
Método assíncrono que encerra a sessão do usuário atualmente autenticado no sistema.

```javascript
import { Auth } from '@growth/commons';

Auth.signOut()
    .then(() => console.log('User signed out'))
    .catch(error => console.log('Sign out error:', error));
```