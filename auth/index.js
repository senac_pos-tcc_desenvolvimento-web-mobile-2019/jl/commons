import { auth } from '../firebase';
import { User } from '../models';


export const signUp = async (email, password, name) => {
  const { user } = await auth().createUserWithEmailAndPassword(email, password);
  console.log('[COMMONS/AUTH] credentials created and authenticated with success');

  const doc = await User.create({
    id: user.uid,
    createdAt: new Date(user.metadata.creationTime),
    email: email.toLowerCase(),
    role: User.initialRole(email),
    active: true,
    name
  });
  console.log('[COMMONS/AUTH] model created with success');
  return User.fromDoc(doc);
};

export const signIn = async (email, password) => {
  const response = await auth().signInWithEmailAndPassword(email, password);
  const doc = await User.find(response.user.uid).get();
  const user = !!doc && User.fromDoc(doc);
  if (user && user.active) {
    return true;
  } else {
    const error = new Error();
    error.code = 'auth/user-disabled';
    throw error;
  }
};

export const signOut = async () => {
  try {
    await auth().signOut();
  } catch (error) {
    console.log('[COMMONS/AUTH] sign out error', error.code);
  }

  return true;
};

export const resetPassword = async (email) => {
  // TODO: set current language!
  // auth().languageCode = new-current-language;
  await auth().sendPasswordResetEmail(email);
  return true;
};

export default {
  signUp,
  signIn,
  signOut,
  resetPassword
};